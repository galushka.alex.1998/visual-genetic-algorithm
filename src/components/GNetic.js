import React from "react";
import { TimeSeriesChart } from "../helpers/chart";
export class GNetic extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputTimeSeries: [],
      chromosomeCount: 0,
      surviveChromosomeCount: 0,
    };
  }

  confirmHandler = () => {
    const { datasets, updateDataSets } = this.props;
    const [input, ...others] = datasets;
    updateDataSets(
      [
        !!input
          ? input
          : new TimeSeriesChart(
              this.state.inputTimeSeries,
              "ts1",
              "rgb(255, 0, 0)"
            ),
        ...others,
      ].filter((d) => !!d)
    );
  };

  render() {
    return (
      <>
        {this.state.inputTimeSeries &&
          this.state.inputTimeSeries.length > 0 && (
            <div>ts: {this.state.inputTimeSeries.join(" ")}</div>
          )}
        <label>
          Input time series
          <input
            type="text"
            onChange={(e) => {
              this.setState({
                inputTimeSeries: new RegExp("^([0-9]|,)+$").test(e.target.value)
                  ? e.target.value.split(",").map((v) => +v)
                  : [],
              });
            }}
          />
        </label>
        <div>Chromosome count: {this.state.chromosomeCount}</div>
        <label>
          Number of chromosome
          <input
            type="number"
            onChange={(e) =>
              this.setState({ chromosomeCount: +e.target.value })
            }
          />
        </label>
        <div>Survive chromosome: {this.state.surviveChromosomeCount}</div>
        <label>
          Number of survive chromosome
          <input
            type="number"
            onChange={(e) =>
              this.setState({ surviveChromosomeCount: +e.target.value })
            }
          />
        </label>
        <button onClick={this.confirmHandler}>Confirm</button>
      </>
    );
  }
}
