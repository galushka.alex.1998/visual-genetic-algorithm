import { useLayoutEffect, useState } from "react";
import { renderChart, TimeSeriesChart } from "./helpers/chart";
import {
  generateSymbolsSequenceEx,
  generateTimeSeriesEx,
} from "./helpers/gmatic/multi-rules-lsystem";
import { gneticProcessor } from "./helpers/gnetic/gnetic";
import { randInt, take } from "./helpers/other";
import { timeSeriesTest } from "./helpers/testData";

let chart = null;

function App() {
  const [chromosomeCount, setChromosomeCount] = useState(50);
  const [surviveChromosomeCount, setSurviveChromosomeCount] = useState(10);
  const [timeSeries] = useState(timeSeriesTest);

  useLayoutEffect(() => {
    if (timeSeries.every((v) => !!v)) {
      if (chart) {
        chart.destroy();
      }
      chart = renderChart(
        [new TimeSeriesChart(timeSeries, "input time series", "rgb(255,0,0)")],
        timeSeries.length
      );
    }
  });

  const run = () => {
    gneticProcessor.start(timeSeries, surviveChromosomeCount);
    setInterval(async () => {
      const bestChromosomes = await gneticProcessor.getIntermediateResults();
      if (chart) {
        chart.destroy();
      }
      chart = renderChart(
        [
          new TimeSeriesChart(timeSeries, "input time series", "rgb(255,0,0)"),
          ...take(
            bestChromosomes.map((chromosome) => {
              const { lsys, value, dValue } = chromosome;
              const sequence = generateSymbolsSequenceEx(
                lsys,
                timeSeries.length
              );
              const ts = generateTimeSeriesEx(sequence, value, dValue);
              return new TimeSeriesChart(
                ts,
                `${lsys.toString()} ${value} ${dValue}`,
                `rgb(${randInt(0, 255)},${randInt(0, 255)},${randInt(0, 255)})`
              );
            }),
            10
          ),
        ],
        timeSeries.length
      );
    }, 100);
  };

  const stop = () => {
    gneticProcessor.stop();
  };

  return (
    <div
      className="App"
      style={{
        width: "1300px",
        height: "400px",
      }}
    >
      <canvas id="ctx"></canvas>
      <div>
        <button onClick={run}>Run</button>
        <button onClick={stop}>Stop</button>
      </div>
    </div>
  );
}

export default App;
