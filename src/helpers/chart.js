import { Chart } from "chart.js";

Array.range = function (min, max, count) {
  const step = (max - min) / count;

  return new Array(count)
    .fill(null)
    .map((v, idx) => Math.round(step * (idx + 1)));
};

const chartOptions = {
  maintainAspectRatio: false,
  spanGaps: false,
  elements: {
    line: {
      tension: 0.000001,
    },
  },
  plugins: {
    filler: {
      propagate: false,
    },
  },
  scales: {
    xAxes: [
      {
        ticks: {
          autoSkip: false,
          maxRotation: 0,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
  title: {
    display: false,
  },
  animation: {
    duration: 0,
  },
};

export function renderChart(datasets, count) {
  if (datasets.length === 0) {
    return;
  }
  const ctx = document.getElementById("ctx").getContext("2d");
  return new Chart(ctx, {
    type: "line",
    data: {
      labels: Array.range(0, count, count),
      datasets: datasets,
    },
    options: chartOptions,
  });
}

export class TimeSeriesChart {
  constructor(data, label, borderColor) {
    this.data = data || [];
    this.label = label || "";
    this.borderColor = borderColor || "";
    this.fill = false;
  }
}
