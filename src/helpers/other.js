export const range = (min, max, count) => {
  return new Array(count)
    .fill(null)
    .map((v, idx) => ((max - min) / count) * (idx + 1));
};

export const xrange = (count) => range(0, 0, count);

export const rand = (min, max) => Math.random() * (max - min) + min;

export const randInt = (min, max) => Math.round(rand(min, max));

export const take = (arr, count) => arr.filter((_, idx) => idx < count);

export const normalize = (x, xMin, xMax, dMin, dMax) =>
  ((x - xMin) * (dMax - dMin)) / (xMax - xMin) + dMin;

export const shuffle = (arr) => arr.sort((a, b) => 0.5 - Math.random());

export const randItem = (arr) => arr[randInt(0, arr.length - 1)];
