import {
  generateSymbolsSequenceEx,
  generateTimeSeriesEx,
  LSys,
  LSysConfig,
} from "./gmatic/multi-rules-lsystem";
import { Rule } from "./gnetic/models";
import { generateSymbolsSequence, generateTimeSeries } from "./lsystem";

const rule = new Rule("f", "f-f++f-f");
const value = 25;
const dValue = 5;
const size = 64;
const sequence = generateSymbolsSequence(rule.axiom, rule, size);
const ts = generateTimeSeries(sequence, value, dValue);

const a = "f";
const f = new Rule("f", "-g+g");
const g = new Rule("g", "f+gg-");

const cfg = new LSysConfig();
const lsys = new LSys(cfg);
lsys.addAxiom(a);
lsys.addRule(f);
lsys.addRule(g);

const seq1 = generateSymbolsSequenceEx(lsys, 50);
const ts1 = generateTimeSeriesEx(seq1, 25, 5);

export const timeSeriesTest = ts1;

// // gg
// // f => f+f
// // g => fgf

// const cfg1 = new LSysConfig();
// cfg1.significant = ["f", "g"];

// const a1 = "gg";
// const f1 = new Rule("f", "f+f");
// const g1 = new Rule("g", "fgf");

// const lsys1 = new LSys(cfg1);

// lsys1.addAxiom(a1);
// lsys1.addRule(f1);
// lsys1.addRule(g1);

// console.log(lsys1, lsys1.isSystemValid());
