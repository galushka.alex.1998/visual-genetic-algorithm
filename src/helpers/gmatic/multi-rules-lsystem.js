import { Rule } from "../gnetic/models";
import { generateRandomRule } from "../lsystem";
import { randInt, randItem, take, xrange } from "../other";

/**
 * @todo move to separate singleton-like module
 */
export class LSysConfig {
  constructor() {
    this.start = "S";
    this.significant = ["f", "g"];
    this.functors = ["+", "-"];
  }
}

export class LSysDescription {
  constructor(axiom = "", rules = []) {
    this.axiom = axiom;
    this.rules = rules;
  }
}

class LSysNode {
  constructor(axiom) {
    this.axiom = axiom;
    this.relationNodes = [];
    this.isVisited = false;
  }
  isRelatedWith(axiom) {
    if (this.isVisited) {
      return false;
    }
    this.isVisited = true;
    const target =
      this.axiom !== axiom
        ? this.relationNodes.find(
            (node) => node.axiom === axiom || node.isRelatedWith(axiom)
          )
        : this;
    return !!target;
  }
}

export class LSys {
  constructor(config: LSysConfig) {
    this.config = config;
    this.axiomRule = null;
    this.rules = [];
  }
  addAxiom(axiom) {
    this.axiomRule = new Rule(this.config.start, axiom);
  }

  addRule(rule: Rule) {
    this.verifyRuleValid(rule);
    this.rules.push(rule);
  }

  verifyRuleValid(rule: Rule) {
    if (!this.config.significant.some((v) => v === rule.axiom)) {
      throw new Error();
    }
  }

  buildGraph() {
    const nodes = [
      this.config.start,
      ...this.config.significant,
      ...this.config.functors,
    ].map((nodeAxiom) => new LSysNode(nodeAxiom));

    const rules = [this.axiomRule, ...this.rules];
    rules.forEach((rule) => {
      const node = nodes.find((node) => node.axiom === rule.axiom);
      Array.from(rule.production).forEach((symbol) => {
        const child = nodes.find(
          (n) =>
            n.axiom === symbol &&
            !node.relationNodes.some((cn) => cn.axiom === n.axiom)
        );
        if (child) {
          node.relationNodes.push(child);
        }
      });
    });

    return nodes[0];
  }

  isSystemValid() {
    let graph = this.buildGraph();
    const literals = [...this.config.significant];
    const rulesContactivityResult = literals.every((literal) => {
      let result = graph.isRelatedWith(literal);
      graph = this.buildGraph();
      return result;
    });

    let isReproducing = true;
    let start = this.axiomRule.production;
    for (let i = 0; i < this.rules.length + 1; i++) {
      let iterationResult = this.iterate(start);

      const significantIteration = Array.from(iterationResult)
        .filter((s) => this.config.significant.some((symbol) => symbol === s))
        .join("");
      const significantStart = Array.from(start)
        .filter((s) => this.config.significant.some((symbol) => symbol === s))
        .join("");
      if (significantIteration.length <= significantStart.length) {
        isReproducing = false;
        break;
      }
      start = iterationResult;
    }

    return rulesContactivityResult && isReproducing;
  }

  iterate(start = "") {
    let axiom = start ? start : this.axiomRule.production;
    let iterationResult = "";
    Array.from(axiom).forEach((symbol) => {
      const targetRule = this.rules.find((r) => r.axiom === symbol);
      iterationResult =
        iterationResult + (targetRule ? targetRule.production : symbol);
    });
    return iterationResult;
  }

  iterateTimes(count) {
    let start = this.axiomRule.production;
    for (let i = 0; i < count; i++) {
      start = this.iterate(start);
    }
    return start;
  }

  static fromDescription(description: LSysDescription, config: LSysConfig) {
    if (!description.axiom || !description.rules) {
      throw new Error("Description is not valid!");
    }
    const lsys = new LSys(config);
    lsys.addAxiom(description.axiom);
    description.rules.forEach((rule) => lsys.addRule(rule));
    return lsys;
  }

  toDescription() {
    return new LSysDescription(
      this.axiomRule.production,
      this.rules.map((r) => new Rule(r.axiom, r.production))
    );
  }

  deepcopy() {
    return LSys.fromDescription(this.toDescription(), this.config);
  }

  toString() {
    return `${this.axiomRule.production} \n ${this.rules
      .map((r) => r.toString())
      .join("")}`;
  }
}

/**
 * @todo rewrite without dependency of generateRandomRule
 */
export const generateRandomRuleEx = (start, signs, funcs, size) => {
  // let production = "";
  // xrange(size).forEach((_, idx) => {
  //   let _signs = [...signs];
  //   let _funcs = [...funcs];
  //   if (idx > 0) {
  //     if (production[idx - 1] === "+") {
  //       _funcs = _funcs.filter((s) => s !== "-");
  //     }
  //     if (production[idx - 1] === "-") {
  //       _funcs = _funcs.filter((s) => s !== "+");
  //     }
  //   }
  //   production = production + randItem([_signs, _funcs][randInt(0, 1)]);
  // });

  // return new Rule(start, production);

  return generateRandomRule(start, [...signs, ...funcs, ""], size);
};

/**
 * @todo generate L-system with constraints such as maxRuleCount, maxRuleLength, e.g.
 */
export const generateRandomlSystem = () => {
  const config = new LSysConfig();
  let lsys = null;
  do {
    const ruleCount = randInt(1, config.significant.length);
    let freeSymbols = take(config.significant, ruleCount);
    config.significant = [...freeSymbols];
    const axiomLength = randInt(1, ruleCount);

    const axiom = xrange(axiomLength)
      .map(() => randItem(freeSymbols))
      .join("");
    const rules = xrange(ruleCount).map((_, idx) => {
      const ruleAxiom = freeSymbols[idx];
      const ruleLength = randInt(2, 12);
      const rule = generateRandomRuleEx(
        ruleAxiom,
        [...freeSymbols, ""],
        config.functors,
        ruleLength
      );
      return rule;
    });

    lsys = new LSys(config);
    lsys.addAxiom(axiom);
    rules.forEach((rule) => {
      lsys.addRule(rule);
    });

    // if (lsys.rules.some((r) => r.production.indexOf("g") !== -1))
    //   console.log(lsys.toString(), lsys.isSystemValid());

    //console.log(lsys.toString());
  } while (!lsys.isSystemValid());

  //console.log(lsys.toString());
  return lsys;
};

export const generateSymbolsSequenceEx = (lsys: LSys, count) => {
  //console.log(lsys.toString());
  let start = lsys.axiomRule.production;
  while (
    Array.from(start).filter((s) =>
      lsys.config.significant.some((symbol) => symbol === s)
    ).length < count
  ) {
    start = lsys.iterate(start);
  }

  //console.log("sseq");

  let result = "";
  let sigCount = 0;
  for (let i = 0; sigCount < count; i++) {
    result = result + start[i];
    if (lsys.config.significant.some((s) => s === start[i])) {
      sigCount++;
    }
  }
  return result;
};

/**
 * @todo rewrite for config using
 */
export const generateTimeSeriesEx = (sequence, value, dValue) => {
  let currentValue = value;
  const ts = [];

  Array.from(sequence).forEach((symbol) => {
    switch (symbol) {
      case "f":
      case "g":
        ts.push(currentValue);
        break;
      case "+":
        currentValue += dValue;
        break;
      case "-":
        currentValue -= dValue;
        break;
    }
  });

  return ts;
};
