import { GNetic, GNeticSettings } from "./gnetic-source";

class GNeticProcessor {
  constructor() {
    this.worker = new GNetic();
  }

  async getIntermediateResults() {
    return new Promise((resolve, reject) => {
      resolve(this.worker.best);
    });
  }

  start(timeSeries, surviveChromosomeCount) {
    const settings = new GNeticSettings();
    settings.timeSeries = timeSeries;
    settings.surviveChromosomeCount = 50;
    settings.crossoveringSize = 15;
    settings.mutationSize = 15;

    this.worker.start(settings);
  }

  stop() {
    this.worker.stop();
  }
}

export const gneticProcessor = new GNeticProcessor();
