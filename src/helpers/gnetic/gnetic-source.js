import { rand, randInt, randItem, take, xrange } from "../other";

import _ from "lodash";
import {
  generateRandomlSystem,
  generateSymbolsSequenceEx,
  generateTimeSeriesEx,
} from "../gmatic/multi-rules-lsystem";

export class GNeticSettings {
  constructor() {
    this.timeSeries = [];
    this.surviveChromosomeCount = 0;
    this.crossoveringSize = 0;
    this.initialPopulationSize = 0;
    this.mutationSize = 0;
  }
}

export class GNetic {
  constructor() {
    this.gneticSettings = null;

    this.doneIterationsCount = 0;
    this.best = [];
    this.population = [];
    this.isRunning = false;

    this.start = this.start.bind(this);
    this.processIteration = this.processIteration.bind(this);
    this.stop = this.stop.bind(this);
  }

  start(settings: GNeticSettings) {
    this.gneticSettings = settings;
    this.isRunning = true;
    this.population = xrange(
      this.gneticSettings.surviveChromosomeCount
    ).map(() =>
      new Chromosome(() => this.gneticSettings.timeSeries).initbase()
    );
    this.processIteration();
  }

  processIteration() {
    if (this.isRunning) {
      this.considerPopulationRebasePhase();
      this.crossoverPhase();
      this.mutationPhase();
      this.cloneEliminationPhase();
      this.selectingPhase();
      setImmediate(this.processIteration);
    }
  }

  considerPopulationRebasePhase() {
    if (
      !this.population.find((c) => c.fitness === 0) &&
      this.population.filter((c) => c.age > 10).length >=
        this.gneticSettings.surviveChromosomeCount
    ) {
      this.population = [
        this.population.sort((a, b) => a.fitness - b.fitness)[0],
        ...xrange(this.gneticSettings.initialPopulationSize).map(() =>
          new Chromosome(() => this.gneticSettings.timeSeries).initbase()
        ),
      ];
    }
  }

  crossoverPhase() {
    const crossoveringChromosomes = xrange(
      this.gneticSettings.crossoveringSize
    ).map(() => this.population[randInt(0, this.population.length - 1)]);

    const children = xrange(this.gneticSettings.crossoveringSize)
      .map(() => {
        const mom = randItem(crossoveringChromosomes);
        const dad = randItem(crossoveringChromosomes);
        return mom.crossover(dad);
      })
      .reduce((acc, curr) => [...acc, ...curr]);

    this.population = [...this.population, ...children];
  }

  cloneEliminationPhase() {
    let uniqPopulation = _.uniqBy(
      this.population.sort((a, b) => a - b),
      (chromosome: Chromosome) => chromosome.toString()
    );

    if (uniqPopulation.length < this.population.length) {
      const diff = this.population.length - uniqPopulation.length;
      const newChromosomes = xrange(diff).map(() =>
        new Chromosome(() => this.gneticSettings.timeSeries).initbase()
      );
      this.population = [...uniqPopulation, ...newChromosomes];
    }
  }

  mutationPhase() {
    const mutants = xrange(this.gneticSettings.mutationSize).map(() =>
      this.population[randInt(0, this.population.length - 1)].mutate()
    );
    this.population = [...this.population, ...mutants].sort(
      (a, b) => a.fitness - b.fitness
    );
  }

  selectingPhase() {
    const sorted = this.population.sort((a, b) => a.fitness - b.fitness);
    this.population = take(sorted, this.gneticSettings.surviveChromosomeCount);
    this.doneIterationsCount++;
    this.population.forEach((c) => (c.age += 1));
    this.best = this.population.map((c) => c.deepcopy());
    //console.log(this.population.map((c) => c.id));
    //console.log(new Set(this.population.map((c) => c.id)));
  }

  stop() {
    this.isRunning = false;
  }
}

class Chromosome {
  /**
   * @todo find best way to extract originTimeSeries
   */
  constructor(originTimeSeriesGetter) {
    this.id = _.uniqueId();
    this.lsys = null;
    this.value = 0;
    this.dValue = 0;
    this.fitness = 0;
    this.age = 0;
    this.originTimeSeriesGetter = originTimeSeriesGetter;

    this.mutate = this.mutate.bind(this);
    this.mutateLSystem = this.mutateLSystem.bind(this);
    this.mutateValue = this.mutateValue.bind(this);
    this.mutateDValue = this.mutateDValue.bind(this);
  }

  initbase() {
    this.lsys = generateRandomlSystem();
    this.value = rand(0, 100);
    this.dValue = rand(0, 100);
    this.calculateFitness();
    return this;
  }

  calculateFitness() {
    const originTimeSeries = this.originTimeSeriesGetter();
    const sequence = generateSymbolsSequenceEx(
      this.lsys,
      originTimeSeries.length
    );
    const timeSeries = generateTimeSeriesEx(sequence, this.value, this.dValue);
    let fitness = 0;
    for (let i = 0; i < originTimeSeries.length; i++) {
      fitness += (originTimeSeries[i] - timeSeries[i]) ** 2;
    }
    this.fitness = fitness;
  }
  crossover(other: Chromosome): Array<Chromosome> {
    const chromosome = new Chromosome(this.originTimeSeriesGetter);
    chromosome.lsys = this.mutateLSystem();
    chromosome.value = other.value;
    chromosome.dValue = other.dValue;
    const chromosome1 = new Chromosome(this.originTimeSeriesGetter);
    chromosome1.lsys = other.mutateLSystem();
    chromosome1.value = this.value;
    chromosome1.dValue = this.dValue;

    chromosome.calculateFitness();
    chromosome1.calculateFitness();

    return [chromosome, chromosome1];
  }

  mutate(): Chromosome {
    const randGen = randInt(0, 2);
    const chromosome = this.deepcopy();
    switch (randGen) {
      case 0:
        chromosome.lsys = this.mutateLSystem();
        break;
      case 1:
        chromosome.value = this.mutateValue();
        break;
      case 2:
        chromosome.dValue = this.mutateDValue();
        break;
    }

    chromosome.calculateFitness();
    return chromosome;
  }
  mutateLSystem() {
    return generateRandomlSystem();
  }
  mutateValue(): Number {
    return rand(20, 30);
  }
  mutateDValue(): Number {
    return rand(0, 10);
  }

  deepcopy() {
    const chromosome = new Chromosome(this.originTimeSeriesGetter);
    chromosome.value = this.value;
    chromosome.dValue = this.dValue;
    chromosome.lsys = this.lsys.deepcopy();
    return chromosome;
  }

  toString() {
    return `${this.lsys.toString()} ${this.value} ${this.dValue}`;
  }
}
