export const WorkerMessageTypes = {
  start: "start",
  stop: "stop",
  getData: "getData",
  replyData: "replyData",
};

export class Rule {
  constructor(axiom, production) {
    this.axiom = axiom;
    this.production = production;
  }

  toString() {
    return `${this.axiom} => ${this.production} \n`;
  }
}
