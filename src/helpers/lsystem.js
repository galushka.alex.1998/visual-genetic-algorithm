import { Rule } from "./gnetic/models";
import { randItem, shuffle, xrange } from "./other";

export const literals = ["f", "+", "-", ""];

export const generateSymbolsSequence = (start, rule: Rule, count) => {
  const { axiom, production } = rule;
  let result = start;
  while (Array.from(result).filter((t) => t === "f").length < count) {
    let iterationResult = "";
    Array.from(result).forEach(
      (symbol) =>
        (iterationResult =
          iterationResult + (symbol === axiom ? production : symbol))
    );
    result = iterationResult;
  }
  return result;
};

export const generateTimeSeries = (sequence, value, dValue) => {
  let currentValue = value;
  const timeSeries = [];
  Array.from(sequence).forEach((symbol) => {
    switch (symbol) {
      case "f":
        timeSeries.push(currentValue);
        break;
      case "-":
        currentValue -= dValue;
        break;
      case "+":
        currentValue += dValue;
        break;
      default:
        break;
    }
  });
  return timeSeries;
};

export const generateRandomRule = (start, literals, size) => {
  let production = "";
  do {
    production = xrange(size)
      .map(() => randItem(literals))
      .join("");
  } while (Array.from(production).filter((s) => s === "f").length < 2);

  const unnecessaryStatements = ["+-", "-+"];
  while (
    unnecessaryStatements.some((statement) => production.includes(statement))
  ) {
    unnecessaryStatements.forEach((statement) => {
      production = production.replace(
        statement,
        xrange(statement.length)
          .map(() => randItem(literals))
          .join("")
      );
    });
  }
  return new Rule(start, production);
};

const filterUnnecessaryStatements = (production) => {
  let statements = ["+-", "-+"];
  while (statements.some((statement) => production.includes(statement))) {
    statements.forEach((statement) => {
      production = production.replace(
        statement,
        xrange(statement.length)
          .map(() => randItem(["f", "+", "-"]))
          .join("")
      );
    });
  }
  return production;
};

export const shuffleRule = (rule: Rule) => {
  let production = shuffle(Array.from(rule.production)).join("");
  production = filterUnnecessaryStatements(production);
  return new Rule(rule.axiom, production);
};
